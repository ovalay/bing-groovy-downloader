import javax.imageio.ImageIO
import java.awt.image.BufferedImage

/**
 * Created by Tim on 14/10/15.
 */

outputDir = "DirectoryThatWillContainDownloadedImages"
params = [format:"xml",
          idx:0,
          n:1,
          mkt:"en-US"]   //format can be xml, js or rss, market can be us, uk, jp, etc..

paramString = params.collect {k,v ->
    "$k=$v"
}.join("&")

def url = "http://www.bing.com/HPImageArchive.aspx?"+paramString

def root = new XmlSlurper().parseText(url.toURL().text)

def imageURL = "http://www.bing.com${root.image.url.text()}"

def name = (outputDir << root.image.startdate.text() << ".jpg")
BufferedImage image = ImageIO.read(imageURL.toURL())
ImageIO.write(image, "jpg", new File(name.toString()))
